-- INNER JOIN
SELECT Pegawai.*, Departemen.*
FROM Pegawai
INNER JOIN Departemen ON Pegawai.ID_Departemen = Departemen.ID_Departemen;

-- INNER JOIN menghasilkan gabungan dari kedua tabel berdasarkan kondisi yang diberikan (di sini, ID_Departemen harus sama). Hanya baris yang memiliki nilai yang sesuai dalam kedua tabel yang akan disertakan dalam hasil.


-- LEFT JOIN
SELECT Pegawai.*, Departemen.*
FROM Pegawai
LEFT JOIN Departemen ON Pegawai.ID_Departemen = Departemen.ID_Departemen;

-- LEFT JOIN menghasilkan semua baris dari tabel kiri (Pegawai), dan baris yang cocok dari tabel kanan (Departemen). Jika tidak ada baris yang cocok dalam tabel kanan, hasilnya akan tetap menampilkan baris dari tabel kiri dengan nilai NULL untuk kolom-kolom dari tabel kanan.


-- RIGHT JOIN
SELECT Pegawai.*, Departemen.*
FROM Pegawai
RIGHT JOIN Departemen ON Pegawai.ID_Departemen = Departemen.ID_Departemen;

-- RIGHT JOIN adalah kebalikan dari LEFT JOIN. Ini menghasilkan semua baris dari tabel kanan (Departemen), dan baris yang cocok dari tabel kiri (Pegawai). Jika tidak ada baris yang cocok dalam tabel kiri, hasilnya akan tetap menampilkan baris dari tabel kanan dengan nilai NULL untuk kolom-kolom dari tabel kiri.

-- FULL JOIN
SELECT Pegawai.*, Departemen.*
FROM Pegawai
FULL JOIN Departemen ON Pegawai.ID_Departemen = Departemen.ID_Departemen;

-- FULL JOIN menghasilkan gabungan dari kedua tabel, dengan baris dari setiap tabel diikutsertakan bahkan jika tidak ada baris yang cocok dalam tabel lainnya. Jika tidak ada nilai yang cocok, maka nilai NULL akan dimasukkan.


Perbedaan utama antara INNER JOIN, LEFT JOIN, RIGHT JOIN, dan FULL JOIN adalah bagaimana mereka menggabungkan baris dari tabel yang berbeda. INNER JOIN hanya menghasilkan baris yang cocok dalam kedua tabel, sedangkan LEFT JOIN, RIGHT JOIN, dan FULL JOIN akan menyertakan baris yang tidak memiliki pasangan dalam tabel lainnya dengan cara yang berbeda.