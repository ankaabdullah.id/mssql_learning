-- Insert data pegawai
INSERT INTO Pegawai (ID_Pegawai, Nama, Jabatan, Tanggal_Masuk, Gaji)
VALUES (1, 'John Doe', 'Manager', '2023-01-01', 5000.00);

-- Insert data departemen
INSERT INTO Departemen (ID_Departemen, Nama_Departemen, Lokasi)
VALUES (1, 'IT Department', 'Jakarta');

-- Insert data penempatan
INSERT INTO Penempatan (ID_Penempatan, ID_Pegawai, ID_Departemen, Tanggal_Mulai)
VALUES (1, 1, 1, '2023-01-01');

-- Insert data proyek
INSERT INTO Proyek (ID_Proyek, Nama_Proyek, Deskripsi, Tanggal_Mulai, Tanggal_Selesai)
VALUES (1, 'Upgrade Infrastruktur', 'Upgrade infrastruktur jaringan dan server', '2023-02-01', '2023-06-01');

-- Insert data penugasan
INSERT INTO Penugasan (ID_Penugasan, ID_Pegawai, ID_Proyek, Tanggal_Mulai, Tanggal_Selesai)
VALUES (1, 1, 1, '2023-02-01', '2023-06-01');



