-- Mengambil lima baris teratas dari tabel 'produk'
SELECT TOP 5 * FROM produk;

-- Mengambil sepuluh baris teratas dari tabel 'pelanggan' berdasarkan kolom 'nama'
SELECT TOP 10 * FROM pelanggan ORDER BY nama;

-- Mengambil tiga baris teratas dari tabel 'pesanan' berdasarkan kolom 'tanggal' secara descending
SELECT TOP 3 * FROM pesanan ORDER BY tanggal DESC;
