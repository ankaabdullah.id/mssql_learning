-- Menampilkan semua data pegawai
SELECT * FROM Pegawai;

-- Menampilkan data pegawai dengan ID_Pegawai = 1
SELECT * FROM Pegawai WHERE ID_Pegawai = 1;

-- Menampilkan data pegawai yang memiliki jabatan 'Manager'
SELECT * FROM Pegawai WHERE Jabatan = 'Manager';







-- Mencari pegawai dengan nama 'John Doe'
SELECT * FROM Pegawai WHERE Nama LIKE '%John Doe%';










-- Menggunakan OFFSET-FETCH untuk pagination, misalnya halaman 2 dengan 5 data per halaman
SELECT * FROM Pegawai ORDER BY ID_Pegawai OFFSET 5 ROWS FETCH NEXT 5 ROWS ONLY;
