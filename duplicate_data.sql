SELECT kolom1, kolom2, ..., COUNT(*) AS jumlah
FROM nama_tabel
GROUP BY kolom1, kolom2, ...
HAVING COUNT(*) > 1;
