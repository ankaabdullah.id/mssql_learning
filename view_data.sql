CREATE VIEW View_Pegawai_Departemen AS
SELECT 
    p.ID_Pegawai,
    p.Nama AS Nama_Pegawai,
    p.Jabatan,
    p.Tanggal_Masuk,
    p.Gaji,
    d.ID_Departemen,
    d.Nama_Departemen,
    d.Lokasi,
    p.createdBy,
    p.createdAt,
    p.updatedBy,
    p.updatedAt,
    p.deletedBy,
    p.deletedAt,
    p.isDelete
FROM 
    Pegawai p
INNER JOIN 
    Penempatan pe ON p.ID_Pegawai = pe.ID_Pegawai
INNER JOIN 
    Departemen d ON pe.ID_Departemen = d.ID_Departemen;
