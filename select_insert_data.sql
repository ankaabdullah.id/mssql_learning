INSERT INTO TabelTujuan (Nama, Jabatan, Gaji)
SELECT Nama, Jabatan, Gaji
FROM Pegawai
WHERE Jabatan = 'Manager';


-- . Query Select Insert dari hasil join beberapa tabel ke tabel atau view baru
INSERT INTO GabunganData (Nama_Pegawai, Nama_Departemen, Lokasi)
SELECT p.Nama, d.Nama_Departemen, d.Lokasi
FROM Pegawai p
JOIN Penempatan pe ON p.ID_Pegawai = pe.ID_Pegawai
JOIN Departemen d ON pe.ID_Departemen = d.ID_Departemen;
