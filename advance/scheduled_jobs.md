Scheduled Jobs adalah fungsi yang memungkinkan Anda menjadwalkan eksekusi otomatis dari tugas-tugas tertentu pada waktu tertentu. Hal ini berguna untuk menjalankan tugas-tugas periodik, seperti pemeliharaan basis data, pembaruan data, atau tugas-tugas batch lainnya. Fungsi ini juga dapat digunakan untuk mengirim email, menghasilkan laporan, atau menjalankan proses ETL (Extract, Transform, Load) secara berkala.

Contoh penggunaan Scheduled Jobs dapat berupa tugas-tugas seperti:

1. Backup database setiap hari pukul 02:00 pagi.
2. Menghapus data yang sudah usang setiap minggu sekali.
3. Mengirim email pemberitahuan kepada pengguna setiap bulan.
4. Mengupdate statistik dalam basis data setiap jam.


`
-- Aktifkan Event Scheduler
SET GLOBAL event_scheduler = ON;

-- Buat Event untuk menjalankan tugas setiap hari pukul 02:00 pagi
CREATE EVENT BackupDatabase
ON SCHEDULE EVERY 1 DAY
STARTS '2024-02-13 02:00:00'
DO
  -- Perintah SQL untuk menjalankan backup database
  BACKUP DATABASE mydatabase TO 'backup_location';
`


Scheduled Jobs adalah fungsi yang memungkinkan Anda menjadwalkan eksekusi otomatis dari tugas-tugas tertentu pada waktu tertentu. Hal ini berguna untuk menjalankan tugas-tugas periodik, seperti pemeliharaan basis data, pembaruan data, atau tugas-tugas batch lainnya. Fungsi ini juga dapat digunakan untuk mengirim email, menghasilkan laporan, atau menjalankan proses ETL (Extract, Transform, Load) secara berkala.

Contoh penggunaan Scheduled Jobs dapat berupa tugas-tugas seperti:

Backup database setiap hari pukul 02:00 pagi.
Menghapus data yang sudah usang setiap minggu sekali.
Mengirim email pemberitahuan kepada pengguna setiap bulan.
Mengupdate statistik dalam basis data setiap jam.
Berikut adalah contoh pembuatan dan pengaturan Scheduled Jobs dalam MySQL menggunakan Event Scheduler:

sql
Copy code
-- Aktifkan Event Scheduler
SET GLOBAL event_scheduler = ON;

-- Buat Event untuk menjalankan tugas setiap hari pukul 02:00 pagi
CREATE EVENT BackupDatabase
ON SCHEDULE EVERY 1 DAY
STARTS '2024-02-13 02:00:00'
DO
  -- Perintah SQL untuk menjalankan backup database
  BACKUP DATABASE mydatabase TO 'backup_location';

# Dalam contoh ini:

- Kode SET GLOBAL event_scheduler = ON; digunakan untuk mengaktifkan Event Scheduler di server MySQL.

- Kita membuat sebuah event bernama BackupDatabase yang akan dijalankan setiap hari sekali pada pukul 02:00 pagi.

- Tugas yang akan dijalankan adalah perintah BACKUP DATABASE mydatabase TO 'backup_location';. Perintah ini harus disesuaikan dengan perintah backup yang sesuai dengan basis data yang Anda gunakan (MySQL tidak memiliki perintah BACKUP DATABASE secara default, ini hanya contoh).

- Anda juga dapat menentukan waktu mulai eksekusi event dengan menggunakan opsi STARTS.

- Setelah event dibuat, Event Scheduler akan secara otomatis menjalankan tugas tersebut sesuai dengan jadwal yang ditetapkan.