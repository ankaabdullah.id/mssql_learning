Stored Procedures adalah sekumpulan perintah SQL yang telah disimpan dalam basis data dan dapat dieksekusi oleh panggilan dari aplikasi atau bahkan oleh perintah SQL itu sendiri. Stored Procedures dapat menerima parameter, melakukan operasi manipulasi data, dan mengembalikan hasil kepada pemanggil.

Berikut adalah contoh sederhana pembuatan dan penggunaan Stored Procedure dalam MySQL:

```
DELIMITER //

CREATE PROCEDURE GetEmployeesByPosition(IN position VARCHAR(100))
BEGIN
    SELECT *
    FROM Pegawai
    WHERE Jabatan = position;
END //

DELIMITER ;
```
Dalam contoh ini:

1. `CREATE PROCEDURE` digunakan untuk mendefinisikan Stored Procedure baru dengan nama `GetEmployeesByPosition`.

2. `IN position VARCHAR(100)` adalah parameter yang diterima oleh Stored Procedure. Dalam kasus ini, kita memiliki parameter `position` yang akan digunakan untuk menyaring data pegawai berdasarkan jabatan.

3. Di dalam blok `BEGIN` dan `END`, kita menulis perintah SQL untuk mengambil data pegawai berdasarkan jabatan yang diterima dari parameter.

4. `DELIMITER //` digunakan untuk mengganti delimiter standar dari `;` menjadi `//`. Hal ini diperlukan karena dalam definisi Stored Procedure kita menggunakan banyak perintah SQL yang dipisahkan oleh `;`. Dengan mengubah delimiter, kita dapat menuliskan definisi Stored Procedure tanpa hambatan.

5. Setelah mendefinisikan Stored Procedure, kita kembali mengatur delimiter ke nilai standar menggunakan `DELIMITER` ;.


Menjalankan Stored Procedure untuk mendapatkan data pegawai berdasarkan jabatan 'Manager':
`CALL GetEmployeesByPosition('Manager');`

Dengan memanggil Stored Procedure `GetEmployeesByPosition` dengan parameter 'Manager', kita dapat mengambil data pegawai yang memiliki jabatan 'Manager' dari tabel `Pegawai`.

Stored Procedures sangat berguna karena memungkinkan Anda untuk menyimpan logika bisnis kompleks dalam basis data, mengurangi lalu lintas jaringan antara aplikasi dan basis data, dan memungkinkan penggunaan ulang kode. Selain itu, Stored Procedures juga dapat meningkatkan keamanan dan memperbaiki kinerja dengan cara caching eksekusi query dan meminimalkan jumlah panggilan database yang diperlukan.