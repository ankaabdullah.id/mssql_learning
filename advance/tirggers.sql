-- Membuat tabel untuk menyimpan log audit
CREATE TABLE HR.Log_Audit (
    ID_Log INT IDENTITY(1,1) PRIMARY KEY,
    Tanggal_Waktu DATETIME DEFAULT GETDATE(),
    Operasi VARCHAR(10),
    ID_Pegawai INT,
    Nama_Pegawai VARCHAR(100),
    Jabatan_Pegawai VARCHAR(100)
);

-- Membuat trigger untuk operasi INSERT
DELIMITER //
CREATE TRIGGER trg_Audit_Insert
AFTER INSERT ON Pegawai
FOR EACH ROW
BEGIN
    INSERT INTO Log_Audit (Operasi, ID_Pegawai, Nama_Pegawai, Jabatan_Pegawai)
    VALUES ('INSERT', NEW.ID_Pegawai, NEW.Nama, NEW.Jabatan);
END//
DELIMITER ;

-- Membuat trigger untuk operasi UPDATE
DELIMITER //
CREATE TRIGGER trg_Audit_Update
AFTER UPDATE ON Pegawai
FOR EACH ROW
BEGIN
    INSERT INTO Log_Audit (Operasi, ID_Pegawai, Nama_Pegawai, Jabatan_Pegawai)
    VALUES ('UPDATE', NEW.ID_Pegawai, NEW.Nama, NEW.Jabatan);
END//
DELIMITER ;

-- Membuat trigger untuk operasi DELETE
DELIMITER //
CREATE TRIGGER trg_Audit_Delete
AFTER DELETE ON Pegawai
FOR EACH ROW
BEGIN
    INSERT INTO Log_Audit (Operasi, ID_Pegawai, Nama_Pegawai, Jabatan_Pegawai)
    VALUES ('DELETE', OLD.ID_Pegawai, OLD.Nama, OLD.Jabatan);
END//
DELIMITER ;
