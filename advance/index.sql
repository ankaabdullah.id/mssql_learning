CREATE INDEX idx_Nama ON Pegawai (Nama);

-- Dengan indeks, basis data dapat langsung melompat ke entri atau entri terdekat dalam indeks Nama yang sesuai dengan nilai 'John Doe'. Ini mengurangi jumlah data yang harus diperiksa secara keseluruhan, sehingga query menjadi lebih cepat.



-- Untuk menghapus indeks dari sebuah tabel, Anda dapat menggunakan perintah DROP INDEX. Berikut adalah contoh penggunaannya:
DROP INDEX idx_Nama ON Pegawai;


-- Anda dapat membuat indeks untuk lebih dari satu kolom dalam sebuah tabel
CREATE INDEX idx_Nama_Jabatan ON Pegawai (Nama, Jabatan);

-- fungsi :
SELECT *
FROM Pegawai
WHERE Nama = 'John Doe' AND Jabatan = 'Manager';

