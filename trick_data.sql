UPDATE Pegawai
SET Nama = 'John Doe',
    Jabatan = 'Manager',
    updatedAt = CURRENT_TIMESTAMP
WHERE ID_Pegawai = 1;


-- Dalam query di atas, kita melakukan update pada kolom Nama dan Jabatan untuk pegawai dengan ID_Pegawai = 1. Kolom updatedAt di-set ke nilai waktu saat ini (CURRENT_TIMESTAMP). Setiap kali baris ini di-update, nilai kolom updatedAt akan diperbarui dengan waktu saat itu juga.




SELECT *
FROM Pegawai
WHERE createdAt BETWEEN '2023-01-01' AND '2023-01-31';

-- Dalam query ini, BETWEEN digunakan untuk memilih semua baris di mana nilai kolom createdAt berada di antara tanggal '2023-01-01' dan '2023-01-31'. Data yang dipilih akan mencakup baris-baris yang memiliki tanggal createdAt di antara kedua tanggal tersebut, termasuk tanggal-tanggal tersebut.





DECLARE @Year INT = 2023;
DECLARE @Quarter INT = 1;

DECLARE @StartDate DATE;
DECLARE @EndDate DATE;

IF @Quarter = 1
BEGIN
    SET @StartDate = DATEFROMPARTS(@Year, 1, 1);
    SET @EndDate = DATEFROMPARTS(@Year, 3, 31);
END
ELSE IF @Quarter = 2
BEGIN
    SET @StartDate = DATEFROMPARTS(@Year, 4, 1);
    SET @EndDate = DATEFROMPARTS(@Year, 6, 30);
END
ELSE IF @Quarter = 3
BEGIN
    SET @StartDate = DATEFROMPARTS(@Year, 7, 1);
    SET @EndDate = DATEFROMPARTS(@Year, 9, 30);
END
ELSE IF @Quarter = 4
BEGIN
    SET @StartDate = DATEFROMPARTS(@Year, 10, 1);
    SET @EndDate = DATEFROMPARTS(@Year, 12, 31);
END

SELECT *
FROM Pegawai
WHERE createdAt BETWEEN @StartDate AND @EndDate;

-- Dalam query ini:

-- Variabel @Year dan @Quarter adalah input dari pengguna.
-- Berdasarkan nilai kuartal yang diberikan, kami menetapkan tanggal mulai dan tanggal akhir yang sesuai untuk mencakup kuartal dan tahun yang diberikan.
-- Kemudian, kita melakukan pencarian berdasarkan rentang tanggal yang ditetapkan menggunakan klausa BETWEEN.

