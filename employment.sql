-- Membuat Database
CREATE DATABASE Perusahaan;

-- Menggunakan Database yang Telah Dibuat
USE Perusahaan;

-- Membuat Tabel Pegawai
CREATE TABLE Pegawai (
    ID_Pegawai INT PRIMARY KEY,
    Nama VARCHAR(100),
    Jabatan VARCHAR(50),
    Tanggal_Masuk DATE,
    Gaji DECIMAL(10, 2)
);

-- Membuat Tabel Departemen
CREATE TABLE Departemen (
    ID_Departemen INT PRIMARY KEY,
    Nama_Departemen VARCHAR(100),
    Lokasi VARCHAR(100)
);

-- Membuat Tabel Penempatan
CREATE TABLE Penempatan (
    ID_Penempatan INT PRIMARY KEY,
    ID_Pegawai INT,
    ID_Departemen INT,
    Tanggal_Mulai DATE,
    FOREIGN KEY (ID_Pegawai) REFERENCES Pegawai(ID_Pegawai),
    FOREIGN KEY (ID_Departemen) REFERENCES Departemen(ID_Departemen)
);

-- Membuat Tabel Proyek
CREATE TABLE Proyek (
    ID_Proyek INT PRIMARY KEY,
    Nama_Proyek VARCHAR(100),
    Deskripsi VARCHAR(255),
    Tanggal_Mulai DATE,
    Tanggal_Selesai DATE
);

-- Membuat Tabel Penugasan
CREATE TABLE Penugasan (
    ID_Penugasan INT PRIMARY KEY,
    ID_Pegawai INT,
    ID_Proyek INT,
    Tanggal_Mulai DATE,
    Tanggal_Selesai DATE,
    FOREIGN KEY (ID_Pegawai) REFERENCES Pegawai(ID_Pegawai),
    FOREIGN KEY (ID_Proyek) REFERENCES Proyek(ID_Proyek)
);


-- Dalam contoh di atas, kita membuat beberapa tabel yang saling berhubungan:

-- Tabel Pegawai: Berisi informasi dasar tentang pegawai.
-- Tabel Departemen: Berisi informasi tentang departemen di perusahaan.
-- Tabel Penempatan: Menyimpan informasi tentang penempatan pegawai di departemen tertentu.
-- Tabel Proyek: Menyimpan informasi tentang proyek-proyek yang ada di perusahaan.
-- Tabel Penugasan: Menyimpan informasi tentang penugasan pegawai pada proyek tertentu.
-- Hubungan antar tabel didefinisikan dengan menggunakan kunci asing (foreign keys) yang menghubungkan kolom-kolom yang sesuai di setiap tabel. Ini memastikan integritas referensial antara data yang saling terkait.



